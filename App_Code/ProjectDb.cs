﻿using System.Collections;
using System.Configuration;
using System.Data.OleDb;

/// <summary>
/// Gets the products from tblProducts
/// </summary>
public class ProjectDb
{
    /// <summary>
    /// Gets all products.
    /// </summary>
    /// <returns>IEnumerable</returns>
    public static IEnumerable GetAllProducts()
    {
        var connection = new OleDbConnection(GetConnectionString());
        const string @select = "SELECT ProductID, ProductName, UnitsInStock FROM tblProducts ORDER BY ProductID"; //"SELECT ProductID FROM tblProducts ORDER BY ProductID";
        var command = new OleDbCommand(select, connection);
        connection.Open();
        var data = command.ExecuteReader();
        return data;
    }

    /// <summary>
    /// Updates the product.
    /// </summary>
    /// <param name="originalProduct">The original product.</param>
    /// <param name="product">The product.</param>
    /// <returns></returns>
    public static int UpdateProduct(Product originalProduct, Product product)
    {
        var connection = new OleDbConnection();
        const string update = "UPDATE tblProducts" +
                              "SET ProductName = @ProductName, UnitsInStock = @UnitsInStock" +
                              "WHERE ProductName = @original_ProductName AND UnitsInStock = @original_UnitsInStock";
        var command = new OleDbCommand(update, connection);
        command.Parameters.AddWithValue("ProductName", product.ProductName);
        command.Parameters.AddWithValue("UnitsInStock", product.UnitsInStock);
        connection.Open();
        var updateCount = command.ExecuteNonQuery();
        connection.Close();
        return updateCount;
    }

    /// <summary>
    /// Gets the connection string.
    /// </summary>
    /// <returns>Conntection string.</returns>
    private static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    }

}