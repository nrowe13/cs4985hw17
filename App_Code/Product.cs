﻿using System;

/// <summary>
/// Stores the Product data from tblProducts
/// </summary>
public class Product
{

    /// <summary>
    /// The _product identifier
    /// </summary>
    private int _productId;
    /// <summary>
    /// The _product name
    /// </summary>
    private string _productName;
    /// <summary>
    /// The _supplier identifier
    /// </summary>
    private int _supplierId;
    /// <summary>
    /// The _category identifier
    /// </summary>
    private int _categoryId;
    /// <summary>
    /// The _quantity per unit
    /// </summary>
    private string _quantityPerUnit;
    /// <summary>
    /// The _unit price
    /// </summary>
    private double _unitPrice;
    /// <summary>
    /// The _units in stock
    /// </summary>
    private int _unitsInStock;
    /// <summary>
    /// The _units on order
    /// </summary>
    private int _unitsOnOrder;
    /// <summary>
    /// The _reorder level
    /// </summary>
    private int _reorderLevel;

    /// <summary>
    /// Gets or sets the product identifier.
    /// </summary>
    /// <value>
    /// The product identifier.
    /// </value>
    /// <exception cref="ArgumentException">ProductID must be a valid integer.</exception>
    public int ProductId
    {
        get { return this._productId; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("ProductID must be a valid integer.");
            }
            this._productId = value;
        }
    }

    /// <summary>
    /// Gets or sets the name of the product.
    /// </summary>
    /// <value>
    /// The name of the product.
    /// </value>
    /// <exception cref="ArgumentException">ProductName must be a valid string.</exception>
    public string ProductName
    {
        get { return this._productName; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("ProductName must be a valid string.");
            }
            this._productName = value;
        }
    }

    /// <summary>
    /// Gets or sets the supplier identifier.
    /// </summary>
    /// <value>
    /// The supplier identifier.
    /// </value>
    /// <exception cref="ArgumentException">SupplierID must be a valid integer.</exception>
    public int SupplierId
    {
        get { return this._supplierId; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("SupplierID must be a valid integer.");
            }
            this._supplierId = value;
        }
    }

    /// <summary>
    /// Gets or sets the category identifier.
    /// </summary>
    /// <value>
    /// The category identifier.
    /// </value>
    /// <exception cref="ArgumentException">CategoryID must be a valid integer.</exception>
    public int CategoryId
    {
        get { return this._categoryId; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("CategoryID must be a valid integer.");
            }
            this._categoryId = value;
        }
    }

    /// <summary>
    /// Gets or sets the quantity per unit.
    /// </summary>
    /// <value>
    /// The quantity per unit.
    /// </value>
    /// <exception cref="ArgumentException">QuantityPerUnit must be a valid string.</exception>
    public string QuantityPerUnit
    {
        get { return this._quantityPerUnit; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("QuantityPerUnit must be a valid string.");
            }
            this._quantityPerUnit = value;
        }
    }

    /// <summary>
    /// Gets or sets the unit price.
    /// </summary>
    /// <value>
    /// The unit price.
    /// </value>
    /// <exception cref="ArgumentException">UnitPrice must be a valid double.</exception>
    public double UnitPrice
    {
        get { return this._unitPrice; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("UnitPrice must be a valid double.");
            }
            this._unitPrice = value;
        }
    }

    /// <summary>
    /// Gets or sets the units in stock.
    /// </summary>
    /// <value>
    /// The units in stock.
    /// </value>
    /// <exception cref="ArgumentException">UnitsInStock must be a valid integer.</exception>
    public int UnitsInStock
    {
        get { return this._unitsInStock; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("UnitsInStock must be a valid integer.");
            }
            this._unitsInStock = value;
        }
    }

    /// <summary>
    /// Gets or sets the units on order.
    /// </summary>
    /// <value>
    /// The units on order.
    /// </value>
    /// <exception cref="ArgumentException">UnitsOnOrder must be a valid integer.</exception>
    public int UnitsOnOrder
    {
        get { return this._unitsOnOrder; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("UnitsOnOrder must be a valid integer.");
            }
            this._unitsOnOrder = value;
        }
    }

    /// <summary>
    /// Gets or sets the reorder level.
    /// </summary>
    /// <value>
    /// The reorder level.
    /// </value>
    /// <exception cref="ArgumentException">ReorderLevel must be a valid integer.</exception>
    public int ReorderLevel
    {
        get { return this._reorderLevel; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("ReorderLevel must be a valid integer.");
            }
            this._reorderLevel = value;
        }
    }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Product"/> is discontinued.
    /// </summary>
    /// <value>
    ///   <c>true</c> if discontinued; otherwise, <c>false</c>.
    /// </value>
    public bool Discontinued { get; set; }
}