﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductMaintenance.aspx.cs" Inherits="ProductMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Chapter 15: Northwind</title>
    <link href="Style/Main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
        <form id="form1" runat="server">
            <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataSourceID="odsProducts" OnRowUpdated="gvProducts_RowUpdated" PageSize="15">
                <Columns>
                    <asp:BoundField DataField="ProductID" HeaderText="ProductID" ReadOnly="True">
                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="ProductName" ValidateRequestMode="Enabled">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbxProductName" runat="server" Text='<%# Bind("ProductName") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvProductName" runat="server" ErrorMessage="This is a required field" ControlToValidate="tbxProductName" Display="Dynamic" CssClass="error" Text="*"></asp:RequiredFieldValidator>
                           </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProductname" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UnitsInStock" ValidateRequestMode="Enabled">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbxUnitsInStock" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUnitsInStock" runat="server" ErrorMessage="This is a required field" ControlToValidate="tbxUnitsInStock" Display="Dynamic" CssClass="error" Text="*"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvUnitsInStock" runat="server" ErrorMessage="Please enter a value greater than or equal to 0" Display="Dynamic" Operator="GreaterThanEqual" ControlToValidate="tbxUnitsInStock" ValueToCompare="0" Text="*" CssClass="error"></asp:CompareValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitsInStock" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="350px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
            <asp:ObjectDataSource ID="odsProducts" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="GetAllProducts" TypeName="ProjectDb" UpdateMethod="UpdateProduct" EnablePaging="False" OnUpdated="odsProducts_Updated">
                <UpdateParameters>
                    <asp:Parameter Name="originalProduct" Type="Object" />
                    <asp:Parameter Name="product" Type="Object" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br/>
            <asp:ValidationSummary ID="vsNorthwind" runat="server" HeaderText="Please correct the following errors: " CssClass="error"/>
            <br/>
            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
        </form>
    </section>
</body>
</html>

