﻿using System;
using System.Web.UI.WebControls;

public partial class ProductMaintenance : System.Web.UI.Page
{

    /// <summary>
    /// Handles the Updated event of the odsProducts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void odsProducts_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        e.AffectedRows = Convert.ToInt32(e.ReturnValue);
    }

    /// <summary>
    /// Handles the RowUpdated event of the gvProducts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvProducts_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occurred.<br /><br />" +
                e.Exception.Message;
            if (e.Exception.InnerException != null)
            {
                this.lblError.Text += "<br />Message: "
                    + e.Exception.InnerException.Message;
                this.lblError.Focus();
            }
                
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category."
                + "<br />Please try again.";
            this.lblError.Focus();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            this.lblError.Text = "";
        }
    }
}